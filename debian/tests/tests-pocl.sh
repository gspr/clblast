#!/bin/sh

set -u
set -e

tests=$(find /usr/bin -type f -name 'clblast_test_*' | sed 's/\/usr\/bin\/clblast_test_//g' | sort)

gcc -std=c11 -Wall -pedantic -o ${AUTOPKGTEST_TMP}/find_pocl debian/tests/find_pocl.c -lOpenCL

pocl_platform_and_device=$(${AUTOPKGTEST_TMP}/find_pocl)
pocl_platform=$(echo $pocl_platform_and_device | awk '{ print $1 }')
pocl_device=$(echo $pocl_platform_and_device | awk '{ print $2 }')

echo "POCL is platform ${pocl_platform}, device ${pocl_device}."

set +e

errors=0

for test in $tests
do
    echo "--------------------------------------------------"
    echo "Running test set ${test}."
    echo "--------------------------------------------------"
    output=$(/usr/bin/clblast_test_${test} -platform $pocl_platform -device $pocl_device)
    if [ $? -ne 0 ]
    then
        errors=$(($errors+1))
    fi
    printf "%s\n" "$output" | ansi2txt
done

echo "--------------------------------------------------"
echo "$errors errors encounted."
echo "--------------------------------------------------"

if [ $errors -ne 0 ]
then
    exit 1
fi

